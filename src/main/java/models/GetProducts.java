package models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetProducts {

	@SerializedName("Response")
	private List<GetProductsResponseItem> response;

	public List<GetProductsResponseItem> getResponse(){
		return response;
	}
}