package models;

import com.google.gson.annotations.SerializedName;

public class GetProductsResponseItem {

	@SerializedName("image")
	private String image;

	@SerializedName("unit")
	private String unit;

	@SerializedName("provider")
	private String provider;

	@SerializedName("price")
	private Object price;

	@SerializedName("title")
	private String title;

	@SerializedName("promoDetails")
	private String promoDetails;

	@SerializedName("brand")
	private String brand;

	@SerializedName("isPromo")
	private boolean isPromo;

	@SerializedName("url")
	private String url;

	public String getImage(){
		return image;
	}

	public String getUnit(){
		return unit;
	}

	public String getProvider(){
		return provider;
	}

	public Object getPrice(){
		return price;
	}

	public String getTitle(){
		return title;
	}

	public String getPromoDetails(){
		return promoDetails;
	}

	public String getBrand(){
		return brand;
	}

	public boolean isIsPromo(){
		return isPromo;
	}

	public String getUrl(){
		return url;
	}
}