Feature: Product Search
  As a user
  I want to search for products
  So that I can find the desired items

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  @SMOKE
  Scenario Outline: Searching for products in product store
    When user call api service to retrieve <product> product
    Then user get results for <product> product as available <availability>
    Examples:
      | product  | availability |
      | apple    | true         |
      | orange   | true         |
      | pasta    | true         |
      | cola     | true         |
      | monkey   | false        |
