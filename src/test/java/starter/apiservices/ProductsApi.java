package starter.apiservices;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.restassured.response.Response;
import models.GetProductsResponseItem;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.hc.core5.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class ProductsApi {
    private static final String BASE_URL = "https://waarkoop-server.herokuapp.com/api/v1/search/demo";
    @Step("Get request")
    public List<GetProductsResponseItem> get(String endpoint) {
        Response response = SerenityRest.given()
                .baseUri(BASE_URL)
                .log().all()
                .contentType("application/json")
                .when()
                .get(endpoint)
                .andReturn();

        if (response.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
            return Collections.emptyList(); // Return an empty list when endpoint is not found
        } else {
            String responseBody = response.getBody().asString();
            Type listType = new TypeToken<List<GetProductsResponseItem>>() {}.getType();
            return new Gson().fromJson(responseBody, listType);
        }
    }
}
