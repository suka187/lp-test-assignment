package starter.stepdefinitions;

import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import starter.apiservices.ProductsApi;

public class ProductsApiActionSteps {
    @Steps
    public ProductsApi productsAPI;

    @When("user call api service to retrieve {word} product")
    public void userCallProductsApi(String product) {
        Serenity.setSessionVariable("AllProducts").to(productsAPI.get(product));
    }
}
