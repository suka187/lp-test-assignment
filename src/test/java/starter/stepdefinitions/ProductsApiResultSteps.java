package starter.stepdefinitions;

import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Then;
import models.GetProductsResponseItem;
import net.serenitybdd.core.Serenity;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class ProductsApiResultSteps {

    @ParameterType(value = "true|True|TRUE|false|False|FALSE")
    public Boolean booleanValue(String value) {
        return Boolean.valueOf(value);
    }

    @Then("user get results for {word} product as available {booleanValue}")
    public void iSeeTheResultsDisplayedFor(String product, boolean availability) {
        List<GetProductsResponseItem> allProducts = Serenity.sessionVariableCalled("AllProducts");
        // Iterate over the products and assert that title contains 'product'
        if (availability) {
            boolean isTitleContainingProduct = false;
            for (GetProductsResponseItem productItem : allProducts) {
                String title = productItem.getTitle();
                if (title.toLowerCase().contains(product)) {
                    isTitleContainingProduct = true;
                    break;
                }
            }
            assertThat(isTitleContainingProduct).isTrue();
        } else {
            assertThat(allProducts).isNullOrEmpty();
        }
    }
}
