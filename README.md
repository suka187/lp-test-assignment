# Serenity Cucumber project
## About
Test project with REST Api testing using Java, Cucumber and Serenity.

Resource used: https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product}

## Get Product Details

This API endpoint retrieves the details of a product based on the provided product name.
### Available products:
    "apple", "pasta", "orange" and "cola"
### Example
**HTTP Method:** GET

**Request:**
/search/demo/orange

**Response:**
- Status Code: 200 (OK)
- Body: JSON object containing the product details, including properties like `provider`, `title`, `price`, `unit` and `url`.

## Prerequisites
- Java jdk 11.0.11 - Make sure you have Java Development Kit (JDK) version 11.0.11 or higher installed on your system. You can download it from [Oracle's website](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html).
- apache-maven-3.8.1 - The project uses Apache Maven as the build and dependency management tool. Make sure you have Apache Maven version 3.8.1 or higher installed. You can download it from the [official Apache Maven website](https://maven.apache.org/download.cgi).

## Getting Started

To get started with the project, follow these steps:

1. Clone the repository:

   ```shell
   git clone https://gitlab.com/suka187/lp-test-assignment.git

2. Install the required dependencies using Maven:
    ```shell
   cd project-name
    mvn clean install

3. Run the project:
    ```shell
   clean verify serenity:aggregate

4. Run test by tag:
    ```shell
   clean verify serenity:aggregate -Dcucumber.filter.tags=SMOKE
The test results will be recorded in the target/site/serenity directory.
## The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
    + java
      + models                 Directory that contains Api models
      + starter
  + test
    + java  
      + starter
        + apiservice            Directory that contains classes related to API services
        + stepdefinitions       Directory that contains the step definition classes for Cucumber scenarios
    + resources  
      + feature.search          Feature files directory
        + GetProducts.feature   Feature containing BDD scenarios
     
```


## The sample scenario
All test scenarios are implemented under one parametrized scenario outline
```Gherkin
  Scenario Outline: Searching for products in product store
    When user call api service to retrieve <product> product
    Then user get results for <product> product as available <availability>
  Examples:
  | product  | availability |
  | apple    | true         |
  | orange   | true         |
  | pasta    | true         |
  | monkey   | false        |
```

## What was refactored and why?
- Name of the API was change since test API is used for retrieving Product data
```java
    @Steps
    public ProductsAPI ProductsAPI;  
```
- Steps are split into two files, action and result files
- All steps definitions are refactored to fit new Scenario outline
- ProductAPI model is created in main/java/models using RoboPojoGenerator
- ProductApi GET call is deserialized based on that model
- The convention was also changed to camelcase.
- Action step use setSessionVariable to store ProductAPI response 
```java
     Serenity.setSessionVariable("AllProducts").to(productsAPI.get(product));
```
- Result steps read this variable and based on the logic make validation